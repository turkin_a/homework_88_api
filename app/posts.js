const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Post = require('../models/Post');
const auth = require('../middleware/auth');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const sortByDate =  data => {
  data.sort((a, b) => {
    return new Date(a.datetime) - new Date(b.datetime)
  });

  return data.reverse();
};

const createRouter = () => {
  router.get('/', (req, res) => {
    Post.find()
      .then(posts => {
        let sortedPosts = sortByDate(posts);

        res.send(sortedPosts)
      })
      .catch(() => res.sendStatus(500));
  });

  router.post('/', [auth, upload.single('image')], (req, res) => {
    if (!req.file || !req.body.description) {
      res.status(400).send({error: 'You should fill all fields'});
    }

    const postData = {
      author: req.body.author,
      userId: req.user._id,
      title: req.body.title,
      description: req.body.description,
      datetime: new Date()
    };

    if (req.file) {
      postData.image = req.file.filename;
    } else {
      postData.image = null;
    }

    const post = new Post(postData);

    post.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get(`/:id`, async (req, res) => {
    let postId = req.params.id;

    let post = await Post.findOne({_id: postId});

    res.send(post);
  });

  return router;
};

module.exports = createRouter;