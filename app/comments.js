const express = require('express');
const Comment = require('../models/Comment');
const auth = require('../middleware/auth');

const createRouter = () => {
  const router = express.Router();

  router.get('/:id', (req, res) => {
    const postId = req.params.id;

    Comment.find({postId})
      .then(comments => res.send(comments))
      .catch(() => res.sendStatus(500));
  });

  router.post('/', auth, (req, res) => {
    const comment = new Comment({
      postId: req.body.postId,
      userId: req.user._id,
      comment: req.body.comment
    });

    comment.author = req.user.username;
    comment.datetime = new Date();

    comment.save()
      .then(comment => res.send(comment))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;