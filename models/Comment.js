const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
  postId: {
    type: String, required: true
  },
  userId: {
    type: String, required: true
  },
  author: {
    type: String, required: true
  },
  comment: {
    type: String, required: true
  },
  datetime: String
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;